-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 21 2020 г., 09:34
-- Версия сервера: 8.0.15
-- Версия PHP: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `gammalon`
--

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_blog_blog`
--

CREATE TABLE `gammalon_blog_blog` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `icon` varchar(250) NOT NULL DEFAULT '',
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `member_status` int(11) NOT NULL DEFAULT '1',
  `post_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_blog_post`
--

CREATE TABLE `gammalon_blog_post` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `publish_time` int(11) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `quote` text,
  `content` text NOT NULL,
  `link` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `comment_status` int(11) NOT NULL DEFAULT '1',
  `create_user_ip` varchar(20) NOT NULL,
  `access_type` int(11) NOT NULL DEFAULT '1',
  `meta_keywords` varchar(250) NOT NULL DEFAULT '',
  `meta_description` varchar(250) NOT NULL DEFAULT '',
  `image` varchar(300) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_blog_post_to_tag`
--

CREATE TABLE `gammalon_blog_post_to_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_blog_tag`
--

CREATE TABLE `gammalon_blog_tag` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_blog_user_to_blog`
--

CREATE TABLE `gammalon_blog_user_to_blog` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `note` varchar(250) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_callback`
--

CREATE TABLE `gammalon_callback` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `url` text,
  `agree` int(11) DEFAULT '0',
  `type` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_category_category`
--

CREATE TABLE `gammalon_category_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_comment_comment`
--

CREATE TABLE `gammalon_comment_comment` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `model` varchar(100) NOT NULL,
  `model_id` int(11) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `text` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) DEFAULT NULL,
  `level` int(11) DEFAULT '0',
  `root` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rgt` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_contentblock_content_block`
--

CREATE TABLE `gammalon_contentblock_content_block` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `content` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gammalon_contentblock_content_block`
--

INSERT INTO `gammalon_contentblock_content_block` (`id`, `name`, `code`, `type`, `content`, `description`, `category_id`, `status`) VALUES
(1, 'logo', 'logo', 3, '<p><a href=\"/\"></a></p><p><a href=\"/\"><span class=\"red\">GAMMALON</span> <br>Москва\r\n</a></p>', '', NULL, 1),
(2, 'mode', 'mode', 1, 'Ежедневно с 10:00 до 20:00', '', NULL, 1),
(3, 'phone', 'phone', 3, '<a href=\"tel:+81 90 9394 3677\">+81 90 9394 3677</a>', '', NULL, 1),
(4, 'email', 'email', 3, '<a href=\"mailto:info@ohonto.com\">info@ohonto.com</a>', '', NULL, 1),
(5, 'address', 'address', 1, '135-0061 Tokyo, Koto-ku Toyosu 3-2-2', '', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_feedback_feedback`
--

CREATE TABLE `gammalon_feedback_feedback` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `answer_user` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone` varchar(150) DEFAULT NULL,
  `theme` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `answer` text NOT NULL,
  `answer_time` datetime DEFAULT NULL,
  `is_faq` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_gallery_gallery`
--

CREATE TABLE `gammalon_gallery_gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `owner` int(11) DEFAULT NULL,
  `preview_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_gallery_image_to_gallery`
--

CREATE TABLE `gammalon_gallery_image_to_gallery` (
  `id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_image_image`
--

CREATE TABLE `gammalon_image_image` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `file` varchar(250) NOT NULL,
  `create_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `alt` varchar(250) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_mail_mail_event`
--

CREATE TABLE `gammalon_mail_mail_event` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gammalon_mail_mail_event`
--

INSERT INTO `gammalon_mail_mail_event` (`id`, `code`, `name`, `description`) VALUES
(1, 'sdelat-zakaz', 'Сделать заказ', ''),
(2, 'uznat-podrobnee', 'Узнать подробнее', ''),
(3, 'zakaz-tovara', 'Заказ товара', '');

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_mail_mail_template`
--

CREATE TABLE `gammalon_mail_mail_template` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `event_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  `from` varchar(150) NOT NULL,
  `to` varchar(150) NOT NULL,
  `theme` text NOT NULL,
  `body` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gammalon_mail_mail_template`
--

INSERT INTO `gammalon_mail_mail_template` (`id`, `code`, `event_id`, `name`, `description`, `from`, `to`, `theme`, `body`, `status`) VALUES
(1, 'zayavka', 1, 'Заявка', '', 'no-reply@dcmr.ru', 'nelyubova_lena@mail.ru', 'Заявка с сайта: theme', '<table>\r\n<tbody>\r\n<tr>\r\n	<td>Имя:\r\n	</td>\r\n	<td>name\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Телефон:\r\n	</td>\r\n	<td>phone\r\n	</td>\r\n</tr>\r\n\r\n</tbody>\r\n</table>', 1),
(2, 'uznat-podrobnee', 2, 'Узнать подробнее', '', 'no-reply@dcmr.ru', 'nelyubova_lena@mail.ru', 'Заявка с сайта: theme', '<table>\r\n<tbody>\r\n<tr>\r\n	<td>Имя:\r\n	</td>\r\n	<td>name\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Телефон:\r\n	</td>\r\n	<td>phone\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Сообщение:\r\n	</td>\r\n	<td>body\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', 1),
(3, 'zakaz-tovara', 3, 'Заказ товара', '', 'no-reply@dcmr.ru', 'nelyubova_lena@mail.ru', 'Заявка: product', '<table>\r\n<tbody>\r\n<tr>\r\n	<td>Имя\r\n	</td>\r\n	<td>name\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>Телефон\r\n	</td>\r\n	<td>phone\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_menu_menu`
--

CREATE TABLE `gammalon_menu_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gammalon_menu_menu`
--

INSERT INTO `gammalon_menu_menu` (`id`, `name`, `code`, `description`, `status`) VALUES
(1, 'Верхнее меню', 'top-menu', 'Основное меню сайта, расположенное сверху в блоке mainmenu.', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_menu_menu_item`
--

CREATE TABLE `gammalon_menu_menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `regular_link` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL,
  `href` varchar(150) NOT NULL,
  `class` varchar(150) DEFAULT NULL,
  `title_attr` varchar(150) DEFAULT NULL,
  `before_link` varchar(150) DEFAULT NULL,
  `after_link` varchar(150) DEFAULT NULL,
  `target` varchar(150) DEFAULT NULL,
  `rel` varchar(150) DEFAULT NULL,
  `condition_name` varchar(150) DEFAULT '0',
  `condition_denial` int(11) DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `entity_module_name` varchar(40) DEFAULT NULL,
  `entity_name` varchar(40) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gammalon_menu_menu_item`
--

INSERT INTO `gammalon_menu_menu_item` (`id`, `parent_id`, `menu_id`, `regular_link`, `title`, `href`, `class`, `title_attr`, `before_link`, `after_link`, `target`, `rel`, `condition_name`, `condition_denial`, `sort`, `status`, `entity_module_name`, `entity_name`, `entity_id`) VALUES
(13, 0, 1, 1, 'О препарате', '#about', 'js-move', '', '', '', '', '', '', 0, 1, 1, '', '', NULL),
(14, 0, 1, 1, 'Показания', '#complex', 'js-move', '', '', '', '', '', '', 0, 2, 1, '', '', NULL),
(15, 0, 1, 1, 'Оригинальность', '#original', 'js-move', '', '', '', '', '', '', 0, 3, 1, '', '', NULL),
(16, 0, 1, 1, 'Стоимость', '#cost', 'js-move', '', '', '', '', '', '', 0, 5, 1, '', '', NULL),
(17, 0, 1, 1, 'Отзывы', '#reviews', 'js-move', '', '', '', '', '', '', 0, 6, 1, '', '', NULL),
(18, 0, 1, 1, 'Способ применения', '#application', 'hide-header js-move', '', '', '', '', '', '', 0, 4, 1, '', '', NULL),
(19, 0, 1, 1, 'Вопросы', '#qustions', 'hide-header js-move', '', '', '', '', '', '', 0, 7, 1, '', '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_migrations`
--

CREATE TABLE `gammalon_migrations` (
  `id` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gammalon_migrations`
--

INSERT INTO `gammalon_migrations` (`id`, `module`, `version`, `apply_time`) VALUES
(1, 'user', 'm000000_000000_user_base', 1603083510),
(2, 'user', 'm131019_212911_user_tokens', 1603083510),
(3, 'user', 'm131025_152911_clean_user_table', 1603083515),
(4, 'user', 'm131026_002234_prepare_hash_user_password', 1603083517),
(5, 'user', 'm131106_111552_user_restore_fields', 1603083517),
(6, 'user', 'm131121_190850_modify_tokes_table', 1603083518),
(7, 'user', 'm140812_100348_add_expire_to_token_table', 1603083518),
(8, 'user', 'm150416_113652_rename_fields', 1603083519),
(9, 'user', 'm151006_000000_user_add_phone', 1603083519),
(10, 'yupe', 'm000000_000000_yupe_base', 1603083521),
(11, 'yupe', 'm130527_154455_yupe_change_unique_index', 1603083521),
(12, 'yupe', 'm150416_125517_rename_fields', 1603083521),
(13, 'yupe', 'm160204_195213_change_settings_type', 1603083521),
(14, 'category', 'm000000_000000_category_base', 1603083523),
(15, 'category', 'm150415_150436_rename_fields', 1603083523),
(16, 'mail', 'm000000_000000_mail_base', 1603083526),
(17, 'image', 'm000000_000000_image_base', 1603083530),
(18, 'image', 'm150226_121100_image_order', 1603083530),
(19, 'image', 'm150416_080008_rename_fields', 1603083530),
(20, 'comment', 'm000000_000000_comment_base', 1603083534),
(21, 'comment', 'm130704_095200_comment_nestedsets', 1603083535),
(22, 'comment', 'm150415_151804_rename_fields', 1603083535),
(23, 'gallery', 'm000000_000000_gallery_base', 1603083538),
(24, 'gallery', 'm130427_120500_gallery_creation_user', 1603083538),
(25, 'gallery', 'm150416_074146_rename_fields', 1603083539),
(26, 'gallery', 'm160514_131314_add_preview_to_gallery', 1603083539),
(27, 'gallery', 'm160515_123559_add_category_to_gallery', 1603083540),
(28, 'gallery', 'm160515_151348_add_position_to_gallery_image', 1603083540),
(29, 'gallery', 'm181224_072816_add_sort_to_gallery', 1603083541),
(30, 'notify', 'm141031_091039_add_notify_table', 1603083543),
(31, 'page', 'm000000_000000_page_base', 1603083548),
(32, 'page', 'm130115_155600_columns_rename', 1603083548),
(33, 'page', 'm140115_083618_add_layout', 1603083548),
(34, 'page', 'm140620_072543_add_view', 1603083548),
(35, 'page', 'm150312_151049_change_body_type', 1603083549),
(36, 'page', 'm150416_101038_rename_fields', 1603083549),
(37, 'page', 'm180224_105407_meta_title_column', 1603083550),
(38, 'page', 'm180421_143324_update_page_meta_column', 1603083550),
(39, 'blog', 'm000000_000000_blog_base', 1603083569),
(40, 'blog', 'm130503_091124_BlogPostImage', 1603083569),
(41, 'blog', 'm130529_151602_add_post_category', 1603083571),
(42, 'blog', 'm140226_052326_add_community_fields', 1603083571),
(43, 'blog', 'm140714_110238_blog_post_quote_type', 1603083572),
(44, 'blog', 'm150406_094809_blog_post_quote_type', 1603083574),
(45, 'blog', 'm150414_180119_rename_date_fields', 1603083574),
(46, 'blog', 'm160518_175903_alter_blog_foreign_keys', 1603083577),
(47, 'blog', 'm180421_143937_update_blog_meta_column', 1603083577),
(48, 'blog', 'm180421_143938_add_post_meta_title_column', 1603083577),
(49, 'feedback', 'm000000_000000_feedback_base', 1603083581),
(50, 'feedback', 'm150415_184108_rename_fields', 1603083581),
(51, 'callback', 'm150926_083350_callback_base', 1603083584),
(52, 'callback', 'm160621_075232_add_date_to_callback', 1603083584),
(53, 'callback', 'm161125_181730_add_url_to_callback', 1603083584),
(54, 'callback', 'm161204_122528_update_callback_encoding', 1603083584),
(55, 'callback', 'm180224_103745_add_agree_column', 1603083584),
(56, 'callback', 'm181213_214512_add_type_column', 1603083584),
(57, 'contentblock', 'm000000_000000_contentblock_base', 1603083585),
(58, 'contentblock', 'm140715_130737_add_category_id', 1603083585),
(59, 'contentblock', 'm150127_130425_add_status_column', 1603083586),
(60, 'menu', 'm000000_000000_menu_base', 1603083588),
(61, 'menu', 'm121220_001126_menu_test_data', 1603083588),
(62, 'menu', 'm160914_134555_fix_menu_item_default_values', 1603083593),
(63, 'menu', 'm181214_110527_menu_item_add_entity_fields', 1603083593),
(64, 'news', 'm000000_000000_news_base', 1603083596),
(65, 'news', 'm150416_081251_rename_fields', 1603083597),
(66, 'news', 'm180224_105353_meta_title_column', 1603083597),
(67, 'news', 'm180421_142416_update_news_meta_column', 1603083597),
(68, 'store', 'm140812_160000_store_attribute_group_base', 1603083598),
(69, 'store', 'm140812_170000_store_attribute_base', 1603083599),
(70, 'store', 'm140812_180000_store_attribute_option_base', 1603083600),
(71, 'store', 'm140813_200000_store_category_base', 1603083602),
(72, 'store', 'm140813_210000_store_type_base', 1603083603),
(73, 'store', 'm140813_220000_store_type_attribute_base', 1603083604),
(74, 'store', 'm140813_230000_store_producer_base', 1603083605),
(75, 'store', 'm140814_000000_store_product_base', 1603083608),
(76, 'store', 'm140814_000010_store_product_category_base', 1603083611),
(77, 'store', 'm140814_000013_store_product_attribute_eav_base', 1603083613),
(78, 'store', 'm140814_000018_store_product_image_base', 1603083613),
(79, 'store', 'm140814_000020_store_product_variant_base', 1603083616),
(80, 'store', 'm141014_210000_store_product_category_column', 1603083620),
(81, 'store', 'm141015_170000_store_product_image_column', 1603083620),
(82, 'store', 'm141218_091834_default_null', 1603083620),
(83, 'store', 'm150210_063409_add_store_menu_item', 1603083621),
(84, 'store', 'm150210_105811_add_price_column', 1603083621),
(85, 'store', 'm150210_131238_order_category', 1603083621),
(86, 'store', 'm150211_105453_add_position_for_product_variant', 1603083621),
(87, 'store', 'm150226_065935_add_product_position', 1603083621),
(88, 'store', 'm150416_112008_rename_fields', 1603083621),
(89, 'store', 'm150417_180000_store_product_link_base', 1603083624),
(90, 'store', 'm150825_184407_change_store_url', 1603083624),
(91, 'store', 'm150907_084604_new_attributes', 1603083627),
(92, 'store', 'm151218_081635_add_external_id_fields', 1603083627),
(93, 'store', 'm151218_082939_add_external_id_ix', 1603083628),
(94, 'store', 'm151218_142113_add_product_index', 1603083628),
(95, 'store', 'm151223_140722_drop_product_type_categories', 1603083629),
(96, 'store', 'm160210_084850_add_h1_and_canonical', 1603083629),
(97, 'store', 'm160210_131541_add_main_image_alt_title', 1603083630),
(98, 'store', 'm160211_180200_add_additional_images_alt_title', 1603083630),
(99, 'store', 'm160215_110749_add_image_groups_table', 1603083631),
(100, 'store', 'm160227_114934_rename_producer_order_column', 1603083631),
(101, 'store', 'm160309_091039_add_attributes_sort_and_search_fields', 1603083631),
(102, 'store', 'm160413_184551_add_type_attr_fk', 1603083632),
(103, 'store', 'm160602_091243_add_position_product_index', 1603083632),
(104, 'store', 'm160602_091909_add_producer_sort_index', 1603083633),
(105, 'store', 'm160713_105449_remove_irrelevant_product_status', 1603083633),
(106, 'store', 'm160805_070905_add_attribute_description', 1603083633),
(107, 'store', 'm161015_121915_change_product_external_id_type', 1603083635),
(108, 'store', 'm161122_090922_add_sort_product_position', 1603083635),
(109, 'store', 'm161122_093736_add_store_layouts', 1603083635),
(110, 'store', 'm181218_121815_store_product_variant_quantity_column', 1603083635),
(111, 'page', 'm180421_143325_add_column_image', 1603089009),
(112, 'page', 'm180421_143326_add_column_body_Short', 1603089010),
(113, 'rbac', 'm140115_131455_auth_item', 1603095721),
(114, 'rbac', 'm140115_132045_auth_item_child', 1603095722),
(115, 'rbac', 'm140115_132319_auth_item_assign', 1603095724),
(116, 'rbac', 'm140702_230000_initial_role_data', 1603095724),
(117, 'video', 'm000000_000000_video_base', 1603095738),
(118, 'video', 'm000000_000001_video_add_column', 1603095738),
(119, 'video', 'm000001_000000_video_category_base', 1603095738),
(120, 'video', 'm000001_000001_add_video', 1603095738),
(121, 'video', 'm000001_000002_alter_video_column_code', 1603095739);

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_news_news`
--

CREATE TABLE `gammalon_news_news` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `date` date NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `short_text` text,
  `full_text` text NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `link` varchar(300) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `meta_title` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_notify_settings`
--

CREATE TABLE `gammalon_notify_settings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `my_post` tinyint(1) NOT NULL DEFAULT '1',
  `my_comment` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_page_page`
--

CREATE TABLE `gammalon_page_page` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `change_user_id` int(11) DEFAULT NULL,
  `title_short` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `body` mediumtext NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `layout` varchar(250) DEFAULT NULL,
  `view` varchar(250) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `icon` varchar(250) DEFAULT NULL,
  `body_short` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gammalon_page_page`
--

INSERT INTO `gammalon_page_page` (`id`, `category_id`, `lang`, `parent_id`, `create_time`, `update_time`, `user_id`, `change_user_id`, `title_short`, `title`, `slug`, `body`, `meta_keywords`, `meta_description`, `status`, `is_protected`, `order`, `layout`, `view`, `meta_title`, `image`, `icon`, `body_short`) VALUES
(1, NULL, 'ru', NULL, '2020-10-19 10:02:19', '2020-10-19 11:22:47', 1, 1, '', 'Гаммалон', 'gammalon', '<p>Главная страница</p>', '', '', 1, 0, 1, '', '', '', NULL, NULL, NULL),
(2, NULL, 'ru', NULL, '2020-10-19 11:31:59', '2020-10-19 16:22:42', 1, 1, '', 'уникальный <br />Японский препарат <br /><span>GAMMALON</span> теперь  <br />в россии', 'unikalnyy-bryaponskiy-preparat-brspangammalonspan-teper-brv-rossii', '<p>Мы доставляем оригинальный препарат по всей России и странам СНГ</p>', '', '', 1, 0, 2, '', '', '', 'fb1597b96444033b65a73fde597b5216.png', NULL, ''),
(3, NULL, 'ru', NULL, '2020-10-19 11:47:51', '2020-10-20 14:52:30', 1, 1, '', '<span>Gammalon<sup>®</sup></span> - это <br />качественное  <br />и эффективное  средство', 'spangammalonspan-eto-brkachestvennoe-bri-effektivnoe-sredstvo', '<p>Основным ингредиентом продукта </p><p>является гамма-аминомасляная </p><p>кислота\r\n(γ-Aminobutyric acid).</p>', '', '', 1, 0, 3, '', '', '', 'f6c5012843bf58c3d5989ae6a4c1def6.png', NULL, '<div class=\"about-list__item\">\r\n				<div class=\"about-list__item-num\">01</div>\r\n				<div class=\"about-list__item-desc\">Способствует выходу из тяжелых состояний, связанных с нарушениями мозгового кровообращения</div>\r\n			</div><div class=\"about-list__item\">\r\n				<div class=\"about-list__item-num\">02</div>\r\n				<div class=\"about-list__item-desc\">Обеспечивает нормализацию динамики нервных процессов, восстанавливает процессы метаболизма в головном мозге</div>\r\n			</div><div class=\"about-list__item\">\r\n				<div class=\"about-list__item-num\">03</div>\r\n				<div class=\"about-list__item-desc\">Повышает продуктивность мышления, улучшает память и оказывает мягкое психостимулирующее действие.  </div>\r\n			</div>'),
(4, NULL, 'ru', NULL, '2020-10-19 12:15:57', '2020-10-19 17:22:41', 1, 1, '', 'Незаменим  <br />В <span>комплексной</span> терапии', 'nezamenim-brv-spankompleksnoyspan-terapii', '<p>GAMMALON – это качественное и эфективное средство основным ингредиентом которого является гамма-аминомасляная кислота (γ-Aminobutyric acid).</p>', '', '', 1, 0, 4, '', '', '', 'e64bfe8d38419f2bbbfee1a830c978cc.png', NULL, '<div class=\"complex-list__item\">\r\n	<div class=\"complex-list__item-icon\">\r\n		<span class=\"icon-1\"></span>\r\n	</div>\r\n	<div class=\"complex-list__item-desc\"><strong>Реабилитация</strong> последствий инсульта, черепно-мозговых травм, нарушений кровообращения головного мозга различного происхождения</div>\r\n</div><div class=\"complex-list__item\">\r\n	<div class=\"complex-list__item-icon\">\r\n		<span class=\"icon-2\"></span>\r\n	</div>\r\n	<div class=\"complex-list__item-desc\"><strong>Восстановление</strong> нарушений памяти, речи и дикции, связанных с врождёнными пороками\r\n</div>\r\n</div><div class=\"complex-list__item\">\r\n	<div class=\"complex-list__item-icon\">\r\n		<span class=\"icon-3\"></span>\r\n	</div>\r\n	<div class=\"complex-list__item-desc\"><strong>Снижение</strong> повышенного артериального давления, гипертонуса</div>\r\n</div>'),
(5, NULL, 'ru', NULL, '2020-10-19 12:17:18', '2020-10-20 14:52:43', 1, 1, 'Мы гарантируем вам надлежащее качество  и оригинальность препарата <span>GAMMALON<sup>®</sup></span>', 'Как определить <br />оригинальность  <br />препарата', 'kak-opredelit-broriginalnost-brpreparata', '<p>в разработке</p>', '', '', 1, 0, 5, '', '', '', '3b243dac1ec2502c1ef5eeef8140fdfd.png', NULL, ''),
(6, NULL, 'ru', NULL, '2020-10-19 12:46:07', '2020-10-20 10:08:53', 1, 1, '', 'Способ применения', 'sposob-primeneniya', '<div class=\"age__item fl fl-ai-fs\">\r\n	<div class=\"age__item-icon\">\r\n		<span class=\"icon-man\"></span>\r\n	</div>\r\n	<div class=\"age__item-desc\">\r\n		<div class=\"age__item-name\">Взрослым</div>\r\n		<p class=\"blue\">от 16 лет - <strong>3-3,75 г. (12-15 таблеток) в день</strong></p>\r\n		<p>Дозировка при укачивании – 0,25 г. (3 раза) в день</p>\r\n	</div>\r\n</div><div class=\"age__item fl fl-ai-fs\">\r\n	<div class=\"age__item-icon\">\r\n		<span class=\"icon-kids\"></span>\r\n	</div>\r\n	<div class=\"age__item-desc\">\r\n		<div class=\"age__item-name\">Детям</div>\r\n		<p class=\"blue\">1-3 года - <strong>1-2 г. (4-8 таблеток) в день</strong></p>\r\n		<p class=\"blue\">4-6 лет – <strong>2-3 г. (8-12 таблеток) в день</strong></p>\r\n		<p class=\"blue\">от 7 лет – <strong>3 г. (12 таблеток) в день</strong></p>\r\n	</div>\r\n</div>', '', '', 1, 0, 6, '', '', '', NULL, NULL, '<div class=\"application-list__item\">\r\n	<div class=\"application-list__item-num\">01</div>\r\n	<div class=\"application-list__item-desc\">Эффект от Gammalon имеет накопительный, продлённый характер, улучшения наблюдаются через 3-4 недели приема.</div>\r\n</div><div class=\"application-list__item\">\r\n	<div class=\"application-list__item-num\">02</div>\r\n	<div class=\"application-list__item-desc\">Таблетки принимают перед приемом пищи, перорально, не разжевывая, запивая достаточным количеством жидкости.</div>\r\n</div><div class=\"application-list__item\">\r\n	<div class=\"application-list__item-num\">03</div>\r\n	<div class=\"application-list__item-desc\">Во время приема Гаммалон® необходимо исключить употребление алкоголя.</div>\r\n</div>'),
(7, NULL, 'ru', NULL, '2020-10-19 13:59:37', '2020-10-19 13:59:37', 1, 1, '', 'Отзывы', 'otzyvy', '<p>В разработке</p>', '', '', 1, 0, 7, '', '', '', NULL, NULL, ''),
(8, NULL, 'ru', 7, '2020-10-19 14:02:11', '2020-10-20 12:13:23', 1, 1, '', 'Дмитрий', 'dmitriy', '<p>Второй раз приобрели гаммалон, сразу 2 упаковки. Все как всегда отлично и оперативно. Гаммалон был отправлен в день оплаты, все чётко и быстро. Спасибо! У нас появились звуки и наконец то ма-ма! На все!  У абсолютно молчащего ребёнка! Звуков много, но пока не привязаны ни к чему. Надеемся и верим...</p>', '', '', 1, 0, 8, '', '', '', 'c9c5224e8205d8b370fc3044ebddf7fd.png', NULL, '<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"17\" height=\"16\" viewBox=\"0 0 17 16\" fill=\"none\">\r\n<path d=\"M8.23128 0.019043L10.3441 5.45186L16.2687 5.85858L11.4397 9.67739L13.1987 15.3071L8.23128 12.092L3.26387 15.3071L4.7996 9.67739L0.193848 5.85858L5.81678 5.45186L8.23128 0.019043Z\" fill=\"#F40034\"/>\r\n</svg><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"17\" height=\"16\" viewBox=\"0 0 17 16\" fill=\"none\">\r\n<path d=\"M8.23128 0.019043L10.3441 5.45186L16.2687 5.85858L11.4397 9.67739L13.1987 15.3071L8.23128 12.092L3.26387 15.3071L4.7996 9.67739L0.193848 5.85858L5.81678 5.45186L8.23128 0.019043Z\" fill=\"#F40034\"/>\r\n</svg><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"17\" height=\"16\" viewBox=\"0 0 17 16\" fill=\"none\">\r\n<path d=\"M8.23128 0.019043L10.3441 5.45186L16.2687 5.85858L11.4397 9.67739L13.1987 15.3071L8.23128 12.092L3.26387 15.3071L4.7996 9.67739L0.193848 5.85858L5.81678 5.45186L8.23128 0.019043Z\" fill=\"#F40034\"/>\r\n</svg><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"17\" height=\"16\" viewBox=\"0 0 17 16\" fill=\"none\">\r\n<path d=\"M8.23128 0.019043L10.3441 5.45186L16.2687 5.85858L11.4397 9.67739L13.1987 15.3071L8.23128 12.092L3.26387 15.3071L4.7996 9.67739L0.193848 5.85858L5.81678 5.45186L8.23128 0.019043Z\" fill=\"#F40034\"/>\r\n</svg><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"17\" height=\"16\" viewBox=\"0 0 17 16\" fill=\"none\">\r\n<path d=\"M8.23128 0.019043L10.3441 5.45186L16.2687 5.85858L11.4397 9.67739L13.1987 15.3071L8.23128 12.092L3.26387 15.3071L4.7996 9.67739L0.193848 5.85858L5.81678 5.45186L8.23128 0.019043Z\" fill=\"#757575\"/>\r\n</svg>'),
(9, NULL, 'ru', 7, '2020-10-19 14:08:23', '2020-10-20 12:13:47', 1, 1, '', 'Андрей', 'andrey', '<p>Второй раз приобрели гаммалон, сразу 2 упаковки. Все как всегда отлично и оперативно. Гаммалон был отправлен в день оплаты, все чётко и быстро. Спасибо!  У нас появились звуки и наконец то ма-ма! На все!  У абсолютно молчащего ребёнка! Звуков много, но пока не привязаны ни к чему. Надеемся и верим...</p>', '', '', 1, 0, 9, '', '', '', 'ccb5ea018a9109060ca437ac2d471b1d.png', NULL, '<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"17\" height=\"16\" viewBox=\"0 0 17 16\" fill=\"none\">\r\n<path d=\"M8.23128 0.019043L10.3441 5.45186L16.2687 5.85858L11.4397 9.67739L13.1987 15.3071L8.23128 12.092L3.26387 15.3071L4.7996 9.67739L0.193848 5.85858L5.81678 5.45186L8.23128 0.019043Z\" fill=\"#F40034\"/>\r\n</svg><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"17\" height=\"16\" viewBox=\"0 0 17 16\" fill=\"none\">\r\n<path d=\"M8.23128 0.019043L10.3441 5.45186L16.2687 5.85858L11.4397 9.67739L13.1987 15.3071L8.23128 12.092L3.26387 15.3071L4.7996 9.67739L0.193848 5.85858L5.81678 5.45186L8.23128 0.019043Z\" fill=\"#F40034\"/>\r\n</svg><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"17\" height=\"16\" viewBox=\"0 0 17 16\" fill=\"none\">\r\n<path d=\"M8.23128 0.019043L10.3441 5.45186L16.2687 5.85858L11.4397 9.67739L13.1987 15.3071L8.23128 12.092L3.26387 15.3071L4.7996 9.67739L0.193848 5.85858L5.81678 5.45186L8.23128 0.019043Z\" fill=\"#F40034\"/>\r\n</svg><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"17\" height=\"16\" viewBox=\"0 0 17 16\" fill=\"none\">\r\n<path d=\"M8.23128 0.019043L10.3441 5.45186L16.2687 5.85858L11.4397 9.67739L13.1987 15.3071L8.23128 12.092L3.26387 15.3071L4.7996 9.67739L0.193848 5.85858L5.81678 5.45186L8.23128 0.019043Z\" fill=\"#F40034\"/>\r\n</svg><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"17\" height=\"16\" viewBox=\"0 0 17 16\" fill=\"none\">\r\n<path d=\"M8.23128 0.019043L10.3441 5.45186L16.2687 5.85858L11.4397 9.67739L13.1987 15.3071L8.23128 12.092L3.26387 15.3071L4.7996 9.67739L0.193848 5.85858L5.81678 5.45186L8.23128 0.019043Z\" fill=\"#757575\"/>\r\n</svg>'),
(10, NULL, 'ru', NULL, '2020-10-19 14:37:41', '2020-10-19 14:37:41', 1, 1, '', 'Вопросы и ответы', 'voprosy-i-otvety', '<p>В разработке</p>', '', '', 1, 0, 10, '', '', '', NULL, NULL, ''),
(11, NULL, 'ru', 10, '2020-10-19 14:38:04', '2020-10-19 14:38:04', 1, 1, '', 'Сколько времени займет доставка?', 'skolko-vremeni-zaymet-dostavka', '<p>Ответ</p>', '', '', 1, 0, 11, '', '', '', NULL, NULL, ''),
(12, NULL, 'ru', 10, '2020-10-19 14:38:17', '2020-10-19 14:38:17', 1, 1, '', 'Настоящий ли это гаммалон из японии?', 'nastoyashchiy-li-eto-gammalon-iz-yaponii', '<p>Ответ</p>', '', '', 1, 0, 12, '', '', '', NULL, NULL, '');

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_store_attribute`
--

CREATE TABLE `gammalon_store_attribute` (
  `id` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `is_filter` smallint(6) NOT NULL DEFAULT '1',
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_store_attribute_group`
--

CREATE TABLE `gammalon_store_attribute_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_store_attribute_option`
--

CREATE TABLE `gammalon_store_attribute_option` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL,
  `value` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_store_category`
--

CREATE TABLE `gammalon_store_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  `external_id` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_canonical` varchar(255) DEFAULT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `image_title` varchar(255) DEFAULT NULL,
  `view` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_store_producer`
--

CREATE TABLE `gammalon_store_producer` (
  `id` int(11) NOT NULL,
  `name_short` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '0',
  `view` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_store_product`
--

CREATE TABLE `gammalon_store_product` (
  `id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `producer_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sku` varchar(100) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `price` decimal(19,3) NOT NULL DEFAULT '0.000',
  `discount_price` decimal(19,3) DEFAULT NULL,
  `discount` decimal(19,3) DEFAULT NULL,
  `description` text,
  `short_description` text,
  `data` text,
  `is_special` tinyint(1) NOT NULL DEFAULT '0',
  `length` decimal(19,3) DEFAULT NULL,
  `width` decimal(19,3) DEFAULT NULL,
  `height` decimal(19,3) DEFAULT NULL,
  `weight` decimal(19,3) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `in_stock` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `average_price` decimal(19,3) DEFAULT NULL,
  `purchase_price` decimal(19,3) DEFAULT NULL,
  `recommended_price` decimal(19,3) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `external_id` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_canonical` varchar(255) DEFAULT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `image_title` varchar(255) DEFAULT NULL,
  `view` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gammalon_store_product`
--

INSERT INTO `gammalon_store_product` (`id`, `type_id`, `producer_id`, `category_id`, `sku`, `name`, `slug`, `price`, `discount_price`, `discount`, `description`, `short_description`, `data`, `is_special`, `length`, `width`, `height`, `weight`, `quantity`, `in_stock`, `status`, `create_time`, `update_time`, `meta_title`, `meta_keywords`, `meta_description`, `image`, `average_price`, `purchase_price`, `recommended_price`, `position`, `external_id`, `title`, `meta_canonical`, `image_alt`, `image_title`, `view`) VALUES
(1, NULL, NULL, NULL, '', '01', '01', '2400.000', NULL, NULL, '<p>Одна упаковка\r\n</p><p class=\"blue\">Gammalon<br />\r\n</p>', '', '<p>В одной упаковке:\r\n</p><table>\r\n<tbody>\r\n<tr>\r\n	<td>10 блистеров<br />\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>100 таблеток\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2020-10-19 13:06:37', '2020-10-20 11:15:30', '', '', '', '3c4fa36c5088930c94f849c1461e7c53.png', NULL, NULL, NULL, 1, NULL, '', '', '', '', ''),
(2, NULL, NULL, NULL, '', '02', '02', '4800.000', '4400.000', NULL, '<p>Две упаковки\r\n</p><p class=\"blue\">Gammalon<br />\r\n</p>', '', '<p>В двух упаковках:\r\n</p><table>\r\n<tbody>\r\n<tr>\r\n	<td>20 блистеров<br />\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>200 таблеток\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2020-10-19 13:07:28', '2020-10-20 11:15:35', '', '', '', '8447fc4d3b2830233509a4cfb95f3160.png', NULL, NULL, NULL, 2, NULL, '', '', '', '', ''),
(3, NULL, NULL, NULL, '', '03', '03', '7200.000', '6100.000', NULL, '<p>Три упаковки\r\n</p><p class=\"blue\">Gammalon\r\n</p>', '', '<p>В трех упаковках:\r\n</p><table>\r\n<tbody>\r\n<tr>\r\n	<td>30 блистеров<br />\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td>300 таблеток\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', 0, NULL, NULL, NULL, NULL, NULL, 1, 1, '2020-10-19 13:08:06', '2020-10-20 11:16:47', '', '', '', '27218c4ae79ee7003984a8c25348946e.png', NULL, NULL, NULL, 3, NULL, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_store_product_attribute_value`
--

CREATE TABLE `gammalon_store_product_attribute_value` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `number_value` double DEFAULT NULL,
  `string_value` varchar(250) DEFAULT NULL,
  `text_value` text,
  `option_value` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_store_product_category`
--

CREATE TABLE `gammalon_store_product_category` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_store_product_image`
--

CREATE TABLE `gammalon_store_product_image` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_store_product_image_group`
--

CREATE TABLE `gammalon_store_product_image_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_store_product_link`
--

CREATE TABLE `gammalon_store_product_link` (
  `id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `linked_product_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_store_product_link_type`
--

CREATE TABLE `gammalon_store_product_link_type` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gammalon_store_product_link_type`
--

INSERT INTO `gammalon_store_product_link_type` (`id`, `code`, `title`) VALUES
(1, 'similar', 'Похожие'),
(2, 'related', 'Сопутствующие');

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_store_product_variant`
--

CREATE TABLE `gammalon_store_product_variant` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_value` varchar(255) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_store_type`
--

CREATE TABLE `gammalon_store_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_store_type_attribute`
--

CREATE TABLE `gammalon_store_type_attribute` (
  `type_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_user_tokens`
--

CREATE TABLE `gammalon_user_tokens` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `expire_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gammalon_user_tokens`
--

INSERT INTO `gammalon_user_tokens` (`id`, `user_id`, `token`, `type`, `status`, `create_time`, `update_time`, `ip`, `expire_time`) VALUES
(2, 1, 'VfPJ6tg44XozlReWGkZfCGSoKtFViFfE', 4, 0, '2020-10-21 09:56:59', '2020-10-21 09:56:59', '127.0.0.1', '2020-10-28 09:56:59');

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_user_user`
--

CREATE TABLE `gammalon_user_user` (
  `id` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `middle_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `nick_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `birth_date` date DEFAULT NULL,
  `site` varchar(250) NOT NULL DEFAULT '',
  `about` varchar(250) NOT NULL DEFAULT '',
  `location` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '2',
  `access_level` int(11) NOT NULL DEFAULT '0',
  `visit_time` datetime DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `avatar` varchar(150) DEFAULT NULL,
  `hash` varchar(255) NOT NULL DEFAULT '8c1c0fcabce0157745d704413211122d0.83574400 1603083515',
  `email_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `phone` char(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gammalon_user_user`
--

INSERT INTO `gammalon_user_user` (`id`, `update_time`, `first_name`, `middle_name`, `last_name`, `nick_name`, `email`, `gender`, `birth_date`, `site`, `about`, `location`, `status`, `access_level`, `visit_time`, `create_time`, `avatar`, `hash`, `email_confirm`, `phone`) VALUES
(1, '2020-10-19 10:00:55', '', '', '', 'admin', 'admin@admin.ru', 0, NULL, '', '', '', 1, 1, '2020-10-21 09:56:59', '2020-10-19 10:00:55', NULL, '$2y$13$4EHnuA7zGDJp5qUO4xUs4OT7.Kg5Tx8g.5sFnnZeCSKrYXTqvLfa6', 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_user_user_auth_assignment`
--

CREATE TABLE `gammalon_user_user_auth_assignment` (
  `itemname` char(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gammalon_user_user_auth_assignment`
--

INSERT INTO `gammalon_user_user_auth_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_user_user_auth_item`
--

CREATE TABLE `gammalon_user_user_auth_item` (
  `name` char(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gammalon_user_user_auth_item`
--

INSERT INTO `gammalon_user_user_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'Admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_user_user_auth_item_child`
--

CREATE TABLE `gammalon_user_user_auth_item_child` (
  `parent` char(64) NOT NULL,
  `child` char(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_video`
--

CREATE TABLE `gammalon_video` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `code` text,
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение(миниатюра)',
  `status` int(11) DEFAULT '0' COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  `category_id` int(11) DEFAULT NULL COMMENT 'Категория',
  `video` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gammalon_video`
--

INSERT INTO `gammalon_video` (`id`, `name`, `code`, `image`, `status`, `position`, `category_id`, `video`) VALUES
(1, '', NULL, '39e1e6ac48e1ace1bcbe1da6decec8aa.jpg', 1, 1, 5, '3c6db849c065f54b5e52d901add69075.mp4');

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_video_category`
--

CREATE TABLE `gammalon_video_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `status` int(11) DEFAULT '0' COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gammalon_yupe_settings`
--

CREATE TABLE `gammalon_yupe_settings` (
  `id` int(11) NOT NULL,
  `module_id` varchar(100) NOT NULL,
  `param_name` varchar(100) NOT NULL,
  `param_value` varchar(500) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gammalon_yupe_settings`
--

INSERT INTO `gammalon_yupe_settings` (`id`, `module_id`, `param_name`, `param_value`, `create_time`, `update_time`, `user_id`, `type`) VALUES
(1, 'yupe', 'siteDescription', 'Уникальный Японский препарат GAMMALON теперь в России', '2020-10-19 10:01:23', '2020-10-19 10:01:23', 1, 1),
(2, 'yupe', 'siteName', 'Gammalon', '2020-10-19 10:01:23', '2020-10-19 10:01:23', 1, 1),
(3, 'yupe', 'siteKeyWords', 'препараты', '2020-10-19 10:01:23', '2020-10-19 10:01:23', 1, 1),
(4, 'yupe', 'email', 'admin@admin.ru', '2020-10-19 10:01:23', '2020-10-19 10:01:23', 1, 1),
(5, 'yupe', 'theme', 'gammalon', '2020-10-19 10:01:23', '2020-10-19 10:01:23', 1, 1),
(6, 'yupe', 'backendTheme', '', '2020-10-19 10:01:23', '2020-10-19 10:01:23', 1, 1),
(7, 'yupe', 'defaultLanguage', 'ru', '2020-10-19 10:01:23', '2020-10-19 10:01:23', 1, 1),
(8, 'yupe', 'defaultBackendLanguage', 'ru', '2020-10-19 10:01:23', '2020-10-19 10:01:23', 1, 1),
(9, 'image', 'uploadPath', 'image', '2020-10-19 10:01:37', '2020-10-19 10:01:37', 1, 1),
(10, 'image', 'allowedExtensions', 'jpg,jpeg,png,gif', '2020-10-19 10:01:37', '2020-10-19 10:01:37', 1, 1),
(11, 'image', 'minSize', '0', '2020-10-19 10:01:37', '2020-10-19 10:01:37', 1, 1),
(12, 'image', 'maxSize', '5242880', '2020-10-19 10:01:37', '2020-10-19 10:01:37', 1, 1),
(13, 'image', 'mainCategory', '', '2020-10-19 10:01:37', '2020-10-19 10:01:37', 1, 1),
(14, 'image', 'mimeTypes', 'image/gif, image/jpeg, image/png', '2020-10-19 10:01:37', '2020-10-19 10:01:37', 1, 1),
(15, 'image', 'width', '2950', '2020-10-19 10:01:37', '2020-10-19 10:01:37', 1, 1),
(16, 'image', 'height', '2950', '2020-10-19 10:01:37', '2020-10-19 10:01:37', 1, 1),
(17, 'homepage', 'mode', '2', '2020-10-19 10:01:48', '2020-10-19 10:01:48', 1, 1),
(18, 'homepage', 'target', '1', '2020-10-19 10:01:48', '2020-10-19 10:02:25', 1, 1),
(19, 'homepage', 'limit', '', '2020-10-19 10:01:48', '2020-10-19 10:01:48', 1, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `gammalon_blog_blog`
--
ALTER TABLE `gammalon_blog_blog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_blog_blog_slug_lang` (`slug`,`lang`),
  ADD KEY `ix_gammalon_blog_blog_create_user` (`create_user_id`),
  ADD KEY `ix_gammalon_blog_blog_update_user` (`update_user_id`),
  ADD KEY `ix_gammalon_blog_blog_status` (`status`),
  ADD KEY `ix_gammalon_blog_blog_type` (`type`),
  ADD KEY `ix_gammalon_blog_blog_create_date` (`create_time`),
  ADD KEY `ix_gammalon_blog_blog_update_date` (`update_time`),
  ADD KEY `ix_gammalon_blog_blog_lang` (`lang`),
  ADD KEY `ix_gammalon_blog_blog_slug` (`slug`),
  ADD KEY `ix_gammalon_blog_blog_category_id` (`category_id`);

--
-- Индексы таблицы `gammalon_blog_post`
--
ALTER TABLE `gammalon_blog_post`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_blog_post_lang_slug` (`slug`,`lang`),
  ADD KEY `ix_gammalon_blog_post_blog_id` (`blog_id`),
  ADD KEY `ix_gammalon_blog_post_create_user_id` (`create_user_id`),
  ADD KEY `ix_gammalon_blog_post_update_user_id` (`update_user_id`),
  ADD KEY `ix_gammalon_blog_post_status` (`status`),
  ADD KEY `ix_gammalon_blog_post_access_type` (`access_type`),
  ADD KEY `ix_gammalon_blog_post_comment_status` (`comment_status`),
  ADD KEY `ix_gammalon_blog_post_lang` (`lang`),
  ADD KEY `ix_gammalon_blog_post_slug` (`slug`),
  ADD KEY `ix_gammalon_blog_post_publish_date` (`publish_time`),
  ADD KEY `ix_gammalon_blog_post_category_id` (`category_id`);

--
-- Индексы таблицы `gammalon_blog_post_to_tag`
--
ALTER TABLE `gammalon_blog_post_to_tag`
  ADD PRIMARY KEY (`post_id`,`tag_id`),
  ADD KEY `ix_gammalon_blog_post_to_tag_post_id` (`post_id`),
  ADD KEY `ix_gammalon_blog_post_to_tag_tag_id` (`tag_id`);

--
-- Индексы таблицы `gammalon_blog_tag`
--
ALTER TABLE `gammalon_blog_tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_blog_tag_tag_name` (`name`);

--
-- Индексы таблицы `gammalon_blog_user_to_blog`
--
ALTER TABLE `gammalon_blog_user_to_blog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_blog_user_to_blog_blog_user_to_blog_u_b` (`user_id`,`blog_id`),
  ADD KEY `ix_gammalon_blog_user_to_blog_blog_user_to_blog_user_id` (`user_id`),
  ADD KEY `ix_gammalon_blog_user_to_blog_blog_user_to_blog_id` (`blog_id`),
  ADD KEY `ix_gammalon_blog_user_to_blog_blog_user_to_blog_status` (`status`),
  ADD KEY `ix_gammalon_blog_user_to_blog_blog_user_to_blog_role` (`role`);

--
-- Индексы таблицы `gammalon_callback`
--
ALTER TABLE `gammalon_callback`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `gammalon_category_category`
--
ALTER TABLE `gammalon_category_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_category_category_alias_lang` (`slug`,`lang`),
  ADD KEY `ix_gammalon_category_category_parent_id` (`parent_id`),
  ADD KEY `ix_gammalon_category_category_status` (`status`);

--
-- Индексы таблицы `gammalon_comment_comment`
--
ALTER TABLE `gammalon_comment_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_gammalon_comment_comment_status` (`status`),
  ADD KEY `ix_gammalon_comment_comment_model_model_id` (`model`,`model_id`),
  ADD KEY `ix_gammalon_comment_comment_model` (`model`),
  ADD KEY `ix_gammalon_comment_comment_model_id` (`model_id`),
  ADD KEY `ix_gammalon_comment_comment_user_id` (`user_id`),
  ADD KEY `ix_gammalon_comment_comment_parent_id` (`parent_id`),
  ADD KEY `ix_gammalon_comment_comment_level` (`level`),
  ADD KEY `ix_gammalon_comment_comment_root` (`root`),
  ADD KEY `ix_gammalon_comment_comment_lft` (`lft`),
  ADD KEY `ix_gammalon_comment_comment_rgt` (`rgt`);

--
-- Индексы таблицы `gammalon_contentblock_content_block`
--
ALTER TABLE `gammalon_contentblock_content_block`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_contentblock_content_block_code` (`code`),
  ADD KEY `ix_gammalon_contentblock_content_block_type` (`type`),
  ADD KEY `ix_gammalon_contentblock_content_block_status` (`status`);

--
-- Индексы таблицы `gammalon_feedback_feedback`
--
ALTER TABLE `gammalon_feedback_feedback`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_gammalon_feedback_feedback_category` (`category_id`),
  ADD KEY `ix_gammalon_feedback_feedback_type` (`type`),
  ADD KEY `ix_gammalon_feedback_feedback_status` (`status`),
  ADD KEY `ix_gammalon_feedback_feedback_isfaq` (`is_faq`),
  ADD KEY `ix_gammalon_feedback_feedback_answer_user` (`answer_user`);

--
-- Индексы таблицы `gammalon_gallery_gallery`
--
ALTER TABLE `gammalon_gallery_gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_gammalon_gallery_gallery_status` (`status`),
  ADD KEY `ix_gammalon_gallery_gallery_owner` (`owner`),
  ADD KEY `fk_gammalon_gallery_gallery_gallery_preview_to_image` (`preview_id`),
  ADD KEY `fk_gammalon_gallery_gallery_gallery_to_category` (`category_id`),
  ADD KEY `ix_gammalon_gallery_gallery_sort` (`sort`);

--
-- Индексы таблицы `gammalon_gallery_image_to_gallery`
--
ALTER TABLE `gammalon_gallery_image_to_gallery`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_gallery_image_to_gallery_gallery_to_image` (`image_id`,`gallery_id`),
  ADD KEY `ix_gammalon_gallery_image_to_gallery_gallery_to_image_image` (`image_id`),
  ADD KEY `ix_gammalon_gallery_image_to_gallery_gallery_to_image_gallery` (`gallery_id`);

--
-- Индексы таблицы `gammalon_image_image`
--
ALTER TABLE `gammalon_image_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_gammalon_image_image_status` (`status`),
  ADD KEY `ix_gammalon_image_image_user` (`user_id`),
  ADD KEY `ix_gammalon_image_image_type` (`type`),
  ADD KEY `ix_gammalon_image_image_category_id` (`category_id`),
  ADD KEY `fk_gammalon_image_image_parent_id` (`parent_id`);

--
-- Индексы таблицы `gammalon_mail_mail_event`
--
ALTER TABLE `gammalon_mail_mail_event`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_mail_mail_event_code` (`code`);

--
-- Индексы таблицы `gammalon_mail_mail_template`
--
ALTER TABLE `gammalon_mail_mail_template`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_mail_mail_template_code` (`code`),
  ADD KEY `ix_gammalon_mail_mail_template_status` (`status`),
  ADD KEY `ix_gammalon_mail_mail_template_event_id` (`event_id`);

--
-- Индексы таблицы `gammalon_menu_menu`
--
ALTER TABLE `gammalon_menu_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_menu_menu_code` (`code`),
  ADD KEY `ix_gammalon_menu_menu_status` (`status`);

--
-- Индексы таблицы `gammalon_menu_menu_item`
--
ALTER TABLE `gammalon_menu_menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_gammalon_menu_menu_item_menu_id` (`menu_id`),
  ADD KEY `ix_gammalon_menu_menu_item_sort` (`sort`),
  ADD KEY `ix_gammalon_menu_menu_item_status` (`status`);

--
-- Индексы таблицы `gammalon_migrations`
--
ALTER TABLE `gammalon_migrations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_migrations_module` (`module`);

--
-- Индексы таблицы `gammalon_news_news`
--
ALTER TABLE `gammalon_news_news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_news_news_alias_lang` (`slug`,`lang`),
  ADD KEY `ix_gammalon_news_news_status` (`status`),
  ADD KEY `ix_gammalon_news_news_user_id` (`user_id`),
  ADD KEY `ix_gammalon_news_news_category_id` (`category_id`),
  ADD KEY `ix_gammalon_news_news_date` (`date`);

--
-- Индексы таблицы `gammalon_notify_settings`
--
ALTER TABLE `gammalon_notify_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_gammalon_notify_settings_user_id` (`user_id`);

--
-- Индексы таблицы `gammalon_page_page`
--
ALTER TABLE `gammalon_page_page`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_page_page_slug_lang` (`slug`,`lang`),
  ADD KEY `ix_gammalon_page_page_status` (`status`),
  ADD KEY `ix_gammalon_page_page_is_protected` (`is_protected`),
  ADD KEY `ix_gammalon_page_page_user_id` (`user_id`),
  ADD KEY `ix_gammalon_page_page_change_user_id` (`change_user_id`),
  ADD KEY `ix_gammalon_page_page_menu_order` (`order`),
  ADD KEY `ix_gammalon_page_page_category_id` (`category_id`);

--
-- Индексы таблицы `gammalon_store_attribute`
--
ALTER TABLE `gammalon_store_attribute`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_store_attribute_name_group` (`name`,`group_id`),
  ADD KEY `ix_gammalon_store_attribute_title` (`title`),
  ADD KEY `fk_gammalon_store_attribute_group` (`group_id`);

--
-- Индексы таблицы `gammalon_store_attribute_group`
--
ALTER TABLE `gammalon_store_attribute_group`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `gammalon_store_attribute_option`
--
ALTER TABLE `gammalon_store_attribute_option`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_gammalon_store_attribute_option_attribute_id` (`attribute_id`),
  ADD KEY `ix_gammalon_store_attribute_option_position` (`position`);

--
-- Индексы таблицы `gammalon_store_category`
--
ALTER TABLE `gammalon_store_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_store_category_alias` (`slug`),
  ADD KEY `ix_gammalon_store_category_parent_id` (`parent_id`),
  ADD KEY `ix_gammalon_store_category_status` (`status`),
  ADD KEY `gammalon_store_category_external_id_ix` (`external_id`);

--
-- Индексы таблицы `gammalon_store_producer`
--
ALTER TABLE `gammalon_store_producer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_gammalon_store_producer_slug` (`slug`),
  ADD KEY `ix_gammalon_store_producer_sort` (`sort`);

--
-- Индексы таблицы `gammalon_store_product`
--
ALTER TABLE `gammalon_store_product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_store_product_alias` (`slug`),
  ADD KEY `ix_gammalon_store_product_status` (`status`),
  ADD KEY `ix_gammalon_store_product_type_id` (`type_id`),
  ADD KEY `ix_gammalon_store_product_producer_id` (`producer_id`),
  ADD KEY `ix_gammalon_store_product_price` (`price`),
  ADD KEY `ix_gammalon_store_product_discount_price` (`discount_price`),
  ADD KEY `ix_gammalon_store_product_create_time` (`create_time`),
  ADD KEY `ix_gammalon_store_product_update_time` (`update_time`),
  ADD KEY `fk_gammalon_store_product_category` (`category_id`),
  ADD KEY `gammalon_store_product_external_id_ix` (`external_id`),
  ADD KEY `ix_gammalon_store_product_sku` (`sku`),
  ADD KEY `ix_gammalon_store_product_name` (`name`),
  ADD KEY `ix_gammalon_store_product_position` (`position`);

--
-- Индексы таблицы `gammalon_store_product_attribute_value`
--
ALTER TABLE `gammalon_store_product_attribute_value`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gammalon_fk_product_attribute_product` (`product_id`),
  ADD KEY `gammalon_fk_product_attribute_attribute` (`attribute_id`),
  ADD KEY `gammalon_fk_product_attribute_option` (`option_value`),
  ADD KEY `gammalon_ix_product_attribute_number_value` (`number_value`),
  ADD KEY `gammalon_ix_product_attribute_string_value` (`string_value`);

--
-- Индексы таблицы `gammalon_store_product_category`
--
ALTER TABLE `gammalon_store_product_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_gammalon_store_product_category_product_id` (`product_id`),
  ADD KEY `ix_gammalon_store_product_category_category_id` (`category_id`);

--
-- Индексы таблицы `gammalon_store_product_image`
--
ALTER TABLE `gammalon_store_product_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_gammalon_store_product_image_product` (`product_id`),
  ADD KEY `fk_gammalon_store_product_image_group` (`group_id`);

--
-- Индексы таблицы `gammalon_store_product_image_group`
--
ALTER TABLE `gammalon_store_product_image_group`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `gammalon_store_product_link`
--
ALTER TABLE `gammalon_store_product_link`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_store_product_link_product` (`product_id`,`linked_product_id`),
  ADD KEY `fk_gammalon_store_product_link_linked_product` (`linked_product_id`),
  ADD KEY `fk_gammalon_store_product_link_type` (`type_id`);

--
-- Индексы таблицы `gammalon_store_product_link_type`
--
ALTER TABLE `gammalon_store_product_link_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_store_product_link_type_code` (`code`),
  ADD UNIQUE KEY `ux_gammalon_store_product_link_type_title` (`title`);

--
-- Индексы таблицы `gammalon_store_product_variant`
--
ALTER TABLE `gammalon_store_product_variant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_gammalon_store_product_variant_product` (`product_id`),
  ADD KEY `idx_gammalon_store_product_variant_attribute` (`attribute_id`),
  ADD KEY `idx_gammalon_store_product_variant_value` (`attribute_value`);

--
-- Индексы таблицы `gammalon_store_type`
--
ALTER TABLE `gammalon_store_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_store_type_name` (`name`);

--
-- Индексы таблицы `gammalon_store_type_attribute`
--
ALTER TABLE `gammalon_store_type_attribute`
  ADD PRIMARY KEY (`type_id`,`attribute_id`),
  ADD KEY `fk_gammalon_store_type_attribute_attribute` (`attribute_id`);

--
-- Индексы таблицы `gammalon_user_tokens`
--
ALTER TABLE `gammalon_user_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_gammalon_user_tokens_user_id` (`user_id`);

--
-- Индексы таблицы `gammalon_user_user`
--
ALTER TABLE `gammalon_user_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_user_user_nick_name` (`nick_name`),
  ADD UNIQUE KEY `ux_gammalon_user_user_email` (`email`),
  ADD KEY `ix_gammalon_user_user_status` (`status`);

--
-- Индексы таблицы `gammalon_user_user_auth_assignment`
--
ALTER TABLE `gammalon_user_user_auth_assignment`
  ADD PRIMARY KEY (`itemname`,`userid`),
  ADD KEY `fk_gammalon_user_user_auth_assignment_user` (`userid`);

--
-- Индексы таблицы `gammalon_user_user_auth_item`
--
ALTER TABLE `gammalon_user_user_auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `ix_gammalon_user_user_auth_item_type` (`type`);

--
-- Индексы таблицы `gammalon_user_user_auth_item_child`
--
ALTER TABLE `gammalon_user_user_auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `fk_gammalon_user_user_auth_item_child_child` (`child`);

--
-- Индексы таблицы `gammalon_video`
--
ALTER TABLE `gammalon_video`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `gammalon_video_category`
--
ALTER TABLE `gammalon_video_category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `gammalon_yupe_settings`
--
ALTER TABLE `gammalon_yupe_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_gammalon_yupe_settings_module_id_param_name_user_id` (`module_id`,`param_name`,`user_id`),
  ADD KEY `ix_gammalon_yupe_settings_module_id` (`module_id`),
  ADD KEY `ix_gammalon_yupe_settings_param_name` (`param_name`),
  ADD KEY `fk_gammalon_yupe_settings_user_id` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `gammalon_blog_blog`
--
ALTER TABLE `gammalon_blog_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_blog_post`
--
ALTER TABLE `gammalon_blog_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_blog_tag`
--
ALTER TABLE `gammalon_blog_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_blog_user_to_blog`
--
ALTER TABLE `gammalon_blog_user_to_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_callback`
--
ALTER TABLE `gammalon_callback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_category_category`
--
ALTER TABLE `gammalon_category_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_comment_comment`
--
ALTER TABLE `gammalon_comment_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_contentblock_content_block`
--
ALTER TABLE `gammalon_contentblock_content_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `gammalon_feedback_feedback`
--
ALTER TABLE `gammalon_feedback_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_gallery_gallery`
--
ALTER TABLE `gammalon_gallery_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_gallery_image_to_gallery`
--
ALTER TABLE `gammalon_gallery_image_to_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_image_image`
--
ALTER TABLE `gammalon_image_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_mail_mail_event`
--
ALTER TABLE `gammalon_mail_mail_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `gammalon_mail_mail_template`
--
ALTER TABLE `gammalon_mail_mail_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `gammalon_menu_menu`
--
ALTER TABLE `gammalon_menu_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `gammalon_menu_menu_item`
--
ALTER TABLE `gammalon_menu_menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `gammalon_migrations`
--
ALTER TABLE `gammalon_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT для таблицы `gammalon_news_news`
--
ALTER TABLE `gammalon_news_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_notify_settings`
--
ALTER TABLE `gammalon_notify_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_page_page`
--
ALTER TABLE `gammalon_page_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `gammalon_store_attribute`
--
ALTER TABLE `gammalon_store_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_store_attribute_group`
--
ALTER TABLE `gammalon_store_attribute_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_store_attribute_option`
--
ALTER TABLE `gammalon_store_attribute_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_store_category`
--
ALTER TABLE `gammalon_store_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_store_producer`
--
ALTER TABLE `gammalon_store_producer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_store_product`
--
ALTER TABLE `gammalon_store_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `gammalon_store_product_attribute_value`
--
ALTER TABLE `gammalon_store_product_attribute_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_store_product_category`
--
ALTER TABLE `gammalon_store_product_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_store_product_image`
--
ALTER TABLE `gammalon_store_product_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_store_product_image_group`
--
ALTER TABLE `gammalon_store_product_image_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_store_product_link`
--
ALTER TABLE `gammalon_store_product_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_store_product_link_type`
--
ALTER TABLE `gammalon_store_product_link_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `gammalon_store_product_variant`
--
ALTER TABLE `gammalon_store_product_variant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_store_type`
--
ALTER TABLE `gammalon_store_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_user_tokens`
--
ALTER TABLE `gammalon_user_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `gammalon_user_user`
--
ALTER TABLE `gammalon_user_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `gammalon_video`
--
ALTER TABLE `gammalon_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `gammalon_video_category`
--
ALTER TABLE `gammalon_video_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gammalon_yupe_settings`
--
ALTER TABLE `gammalon_yupe_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `gammalon_blog_blog`
--
ALTER TABLE `gammalon_blog_blog`
  ADD CONSTRAINT `fk_gammalon_blog_blog_category_id` FOREIGN KEY (`category_id`) REFERENCES `gammalon_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_gammalon_blog_blog_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `gammalon_user_user` (`id`) ON DELETE RESTRICT,
  ADD CONSTRAINT `fk_gammalon_blog_blog_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `gammalon_user_user` (`id`) ON DELETE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `gammalon_blog_post`
--
ALTER TABLE `gammalon_blog_post`
  ADD CONSTRAINT `fk_gammalon_blog_post_blog` FOREIGN KEY (`blog_id`) REFERENCES `gammalon_blog_blog` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_gammalon_blog_post_category_id` FOREIGN KEY (`category_id`) REFERENCES `gammalon_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_gammalon_blog_post_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `gammalon_user_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_gammalon_blog_post_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `gammalon_user_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `gammalon_blog_post_to_tag`
--
ALTER TABLE `gammalon_blog_post_to_tag`
  ADD CONSTRAINT `fk_gammalon_blog_post_to_tag_post_id` FOREIGN KEY (`post_id`) REFERENCES `gammalon_blog_post` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_gammalon_blog_post_to_tag_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `gammalon_blog_tag` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_blog_user_to_blog`
--
ALTER TABLE `gammalon_blog_user_to_blog`
  ADD CONSTRAINT `fk_gammalon_blog_user_to_blog_blog_user_to_blog_blog_id` FOREIGN KEY (`blog_id`) REFERENCES `gammalon_blog_blog` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_gammalon_blog_user_to_blog_blog_user_to_blog_user_id` FOREIGN KEY (`user_id`) REFERENCES `gammalon_user_user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_category_category`
--
ALTER TABLE `gammalon_category_category`
  ADD CONSTRAINT `fk_gammalon_category_category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `gammalon_category_category` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `gammalon_comment_comment`
--
ALTER TABLE `gammalon_comment_comment`
  ADD CONSTRAINT `fk_gammalon_comment_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `gammalon_comment_comment` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_gammalon_comment_comment_user_id` FOREIGN KEY (`user_id`) REFERENCES `gammalon_user_user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_feedback_feedback`
--
ALTER TABLE `gammalon_feedback_feedback`
  ADD CONSTRAINT `fk_gammalon_feedback_feedback_answer_user` FOREIGN KEY (`answer_user`) REFERENCES `gammalon_user_user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_gammalon_feedback_feedback_category` FOREIGN KEY (`category_id`) REFERENCES `gammalon_category_category` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `gammalon_gallery_gallery`
--
ALTER TABLE `gammalon_gallery_gallery`
  ADD CONSTRAINT `fk_gammalon_gallery_gallery_gallery_preview_to_image` FOREIGN KEY (`preview_id`) REFERENCES `gammalon_image_image` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_gammalon_gallery_gallery_gallery_to_category` FOREIGN KEY (`category_id`) REFERENCES `gammalon_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_gammalon_gallery_gallery_owner` FOREIGN KEY (`owner`) REFERENCES `gammalon_user_user` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `gammalon_gallery_image_to_gallery`
--
ALTER TABLE `gammalon_gallery_image_to_gallery`
  ADD CONSTRAINT `fk_gammalon_gallery_image_to_gallery_gallery_to_image_gallery` FOREIGN KEY (`gallery_id`) REFERENCES `gammalon_gallery_gallery` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_gammalon_gallery_image_to_gallery_gallery_to_image_image` FOREIGN KEY (`image_id`) REFERENCES `gammalon_image_image` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_image_image`
--
ALTER TABLE `gammalon_image_image`
  ADD CONSTRAINT `fk_gammalon_image_image_category_id` FOREIGN KEY (`category_id`) REFERENCES `gammalon_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_gammalon_image_image_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `gammalon_image_image` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_gammalon_image_image_user_id` FOREIGN KEY (`user_id`) REFERENCES `gammalon_user_user` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `gammalon_mail_mail_template`
--
ALTER TABLE `gammalon_mail_mail_template`
  ADD CONSTRAINT `fk_gammalon_mail_mail_template_event_id` FOREIGN KEY (`event_id`) REFERENCES `gammalon_mail_mail_event` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_menu_menu_item`
--
ALTER TABLE `gammalon_menu_menu_item`
  ADD CONSTRAINT `fk_gammalon_menu_menu_item_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `gammalon_menu_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_news_news`
--
ALTER TABLE `gammalon_news_news`
  ADD CONSTRAINT `fk_gammalon_news_news_category_id` FOREIGN KEY (`category_id`) REFERENCES `gammalon_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_gammalon_news_news_user_id` FOREIGN KEY (`user_id`) REFERENCES `gammalon_user_user` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `gammalon_notify_settings`
--
ALTER TABLE `gammalon_notify_settings`
  ADD CONSTRAINT `fk_gammalon_notify_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `gammalon_user_user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_page_page`
--
ALTER TABLE `gammalon_page_page`
  ADD CONSTRAINT `fk_gammalon_page_page_category_id` FOREIGN KEY (`category_id`) REFERENCES `gammalon_category_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_gammalon_page_page_change_user_id` FOREIGN KEY (`change_user_id`) REFERENCES `gammalon_user_user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_gammalon_page_page_user_id` FOREIGN KEY (`user_id`) REFERENCES `gammalon_user_user` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `gammalon_store_attribute`
--
ALTER TABLE `gammalon_store_attribute`
  ADD CONSTRAINT `fk_gammalon_store_attribute_group` FOREIGN KEY (`group_id`) REFERENCES `gammalon_store_attribute_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_store_attribute_option`
--
ALTER TABLE `gammalon_store_attribute_option`
  ADD CONSTRAINT `fk_gammalon_store_attribute_option_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `gammalon_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_store_category`
--
ALTER TABLE `gammalon_store_category`
  ADD CONSTRAINT `fk_gammalon_store_category_parent` FOREIGN KEY (`parent_id`) REFERENCES `gammalon_store_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_store_product`
--
ALTER TABLE `gammalon_store_product`
  ADD CONSTRAINT `fk_gammalon_store_product_category` FOREIGN KEY (`category_id`) REFERENCES `gammalon_store_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_gammalon_store_product_producer` FOREIGN KEY (`producer_id`) REFERENCES `gammalon_store_producer` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_gammalon_store_product_type` FOREIGN KEY (`type_id`) REFERENCES `gammalon_store_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_store_product_attribute_value`
--
ALTER TABLE `gammalon_store_product_attribute_value`
  ADD CONSTRAINT `gammalon_fk_product_attribute_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `gammalon_store_attribute` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `gammalon_fk_product_attribute_option` FOREIGN KEY (`option_value`) REFERENCES `gammalon_store_attribute_option` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `gammalon_fk_product_attribute_product` FOREIGN KEY (`product_id`) REFERENCES `gammalon_store_product` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_store_product_category`
--
ALTER TABLE `gammalon_store_product_category`
  ADD CONSTRAINT `fk_gammalon_store_product_category_category` FOREIGN KEY (`category_id`) REFERENCES `gammalon_store_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_gammalon_store_product_category_product` FOREIGN KEY (`product_id`) REFERENCES `gammalon_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_store_product_image`
--
ALTER TABLE `gammalon_store_product_image`
  ADD CONSTRAINT `fk_gammalon_store_product_image_group` FOREIGN KEY (`group_id`) REFERENCES `gammalon_store_product_image_group` (`id`) ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_gammalon_store_product_image_product` FOREIGN KEY (`product_id`) REFERENCES `gammalon_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_store_product_link`
--
ALTER TABLE `gammalon_store_product_link`
  ADD CONSTRAINT `fk_gammalon_store_product_link_linked_product` FOREIGN KEY (`linked_product_id`) REFERENCES `gammalon_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_gammalon_store_product_link_product` FOREIGN KEY (`product_id`) REFERENCES `gammalon_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_gammalon_store_product_link_type` FOREIGN KEY (`type_id`) REFERENCES `gammalon_store_product_link_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_store_product_variant`
--
ALTER TABLE `gammalon_store_product_variant`
  ADD CONSTRAINT `fk_gammalon_store_product_variant_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `gammalon_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_gammalon_store_product_variant_product` FOREIGN KEY (`product_id`) REFERENCES `gammalon_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_store_type_attribute`
--
ALTER TABLE `gammalon_store_type_attribute`
  ADD CONSTRAINT `fk_gammalon_store_type_attribute_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `gammalon_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_gammalon_store_type_attribute_type` FOREIGN KEY (`type_id`) REFERENCES `gammalon_store_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_user_tokens`
--
ALTER TABLE `gammalon_user_tokens`
  ADD CONSTRAINT `fk_gammalon_user_tokens_user_id` FOREIGN KEY (`user_id`) REFERENCES `gammalon_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_user_user_auth_assignment`
--
ALTER TABLE `gammalon_user_user_auth_assignment`
  ADD CONSTRAINT `fk_gammalon_user_user_auth_assignment_item` FOREIGN KEY (`itemname`) REFERENCES `gammalon_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_gammalon_user_user_auth_assignment_user` FOREIGN KEY (`userid`) REFERENCES `gammalon_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_user_user_auth_item_child`
--
ALTER TABLE `gammalon_user_user_auth_item_child`
  ADD CONSTRAINT `fk_gammalon_user_user_auth_item_child_child` FOREIGN KEY (`child`) REFERENCES `gammalon_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_gammalon_user_user_auth_itemchild_parent` FOREIGN KEY (`parent`) REFERENCES `gammalon_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gammalon_yupe_settings`
--
ALTER TABLE `gammalon_yupe_settings`
  ADD CONSTRAINT `fk_gammalon_yupe_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `gammalon_user_user` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
