<?php

use YandexCheckout\Client;
use YandexCheckout\Model\Notification\NotificationSucceeded;
use YandexCheckout\Model\Notification\NotificationWaitingForCapture;
use YandexCheckout\Model\NotificationEventType;

/**
 * Class Ycash
 *
 */
class Ycash extends CApplicationComponent{
    
    /**
    * shop
    */
    public $shopId;
    
    /**
    * testShopId
    */
    public $testShopId;
    
    /**
    * secret key
    */
    public $secretKey;
    
    /**
    * secret key
    */
    public $testSecretKey;
    
    /**
    *
    * Чтобы принять оплату, необходимо создать объект платежа — Payment
    . Он содержит всю необходимую информацию для проведения оплаты (сумму, валюту и статус). У платежа линейный жизненный цикл, он последовательно переходит из статуса в статус.
    В ответ на запрос придет объект платежа в актуальном статусе.
    *
    */
    public function createPayment($payment){
        
        $client = new Client();
        
        //$client->setAuth($this->shopId, $this->secretKey);
        $client->setAuth($this->testShopId, $this->testSecretKey);
        
        $payment = $client->createPayment(
            [
                'amount' => [
                    'value' => $payment->amount_payment,
                    'currency' => 'RUB',
                ],

                'confirmation' => [
                    'type' => 'redirect',
                    'return_url' => Yii::app()->createAbsoluteUrl('/inshine/inshine/statusPayment', [ 'id' => $payment->id ])
                    //'return_url' => Yii::app()->createAbsoluteUrl('/inshine/inshine/processPayment', [], 'https')
                ],
                
                'capture' => true,
                'description' => $payment->course->name_course,
            ],
            uniqid('', true)
        );
 
        return $payment;
    }
    
    public function processPayment($requestBody){
        
        try {
            $notification = ($requestBody['event'] === NotificationEventType::PAYMENT_SUCCEEDED)
            ? new NotificationSucceeded($requestBody) : new NotificationWaitingForCapture($requestBody);
            
            $payment = new PaymentsClients;
            
            $status = ($requestBody['event'] === NotificationEventType::PAYMENT_SUCCEEDED) ? PaymentsClients::PAYMENT_SUCCEEDED : PaymentsClients::PAYMENT_WAITING_FOR_CAPTURE;
            
            $payment->processPayment($notification->getObject(), $requestBody, $status);
            
        } catch (Exception $e) {
            // Обработка ошибок при неверных данных
        }
    }
    
    
}