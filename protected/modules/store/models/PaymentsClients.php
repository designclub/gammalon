<?php

/**
 * This is the model class for table "{{payments_clients}}".
 *
 * The followings are the available columns in table '{{payments_clients}}':
 * @property integer $id
 * @property string $email
 * @property string $phone
 * @property string $name
 * @property integer $product_id
 * @property string $date_payment
 * @property string $amount_payment
 * @property string $number_payment
 * @property string $number_card
 * @property integer $status_payment
 * @property string $errorMessage
 * @property string $errorCode
 * @property string $cardholderName
 * @property string $comment
 */
class PaymentsClients extends \yupe\models\YModel
{
    
    /**
    *
    * платеж создан и ожидает действий от пользователя.
    * Из этого статуса платеж может перейти в succeeded, 
    * waiting_for_capture (при двухстадийной оплате) 
    * или canceled (если что-то пошло не так).
    *
    */
    const PAYMENT_PENDING = 1;
    
    /**
    *
    * платеж оплачен, деньги авторизованы и ожидают списания. 
    * Из этого статуса платеж может перейти в succeeded (если вы списали оплату)
    * или canceled (если вы отменили платеж или что-то пошло не так).
    *
    */
    const PAYMENT_WAITING_FOR_CAPTURE = 2;

    /**
    *
    * платеж успешно завершен, деньги будут перечислены на  расчетный счет.
    * Это финальный и неизменяемый статус.
    *
    */
    const PAYMENT_SUCCEEDED = 3;
    
        
    /**
     * @var null
     */
    private $_validCoupons = null;
    
    /**
    *
    * платеж отменен. 
    * Вы увидите этот статус, если вы отменили платеж самостоятельно, истекло время на принятие платежа
    * или платеж был отклонен Яндекс.Кассой или платежным провайдером.
    * Это финальный и неизменяемый статус.
    *
    */
    const PAYMENT_CANCELED = 4;
    
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{payments_clients}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_id, amount_payment', 'required'),
			array('product_id, status_payment', 'numerical', 'integerOnly'=>true),
			array('email, phone, name, errorMessage, cardholderName', 'length', 'max'=>1000),
			array('amount_payment, errorCode', 'length', 'max'=>10),
			array('number_payment', 'length', 'max'=>50),
			array('number_card', 'length', 'max'=>30),
			array('comment, amount_payment, phone, email, name', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, phone, name, product_id, date_payment, amount_payment, number_payment, number_card, status_payment, errorMessage, errorCode, cardholderName, comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
    
    public function beforeSave() {
        
        $this->date_payment = new CDbExpression('NOW()');
        
        return parent::beforeSave();
    }
        
    public function afterSave(){

        if($this->status_payment == self::PAYMENT_SUCCEEDED){
            //отправка уведомления
/*                Yii::app()->eventManager->fire(
                InshineEvents::SUCCESS_PAYMENT, 
                new InshinePaymentEvent($this, $this->loadUser($this->user_id))
            );

            //отправка уведомления администратору
            Yii::app()->eventManager->fire(
                InshineEvents::ADMIN_PAYMENT_CLIENT, 
                new InshinePaymentEvent($this, $this->loadUser($this->user_id))
            );*/
        }
        
        return parent::afterSave();
    }
    
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'phone' => 'Phone',
			'name' => 'Name',
			'product_id' => 'Product',
			'date_payment' => 'Date Payment',
			'amount_payment' => 'Amount Payment',
			'number_payment' => 'Number Payment',
			'number_card' => 'Number Card',
			'status_payment' => 'Status Payment',
			'errorMessage' => 'Error Message',
			'errorCode' => 'Error Code',
			'cardholderName' => 'Cardholder Name',
			'comment' => 'Comment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('date_payment',$this->date_payment,true);
		$criteria->compare('amount_payment',$this->amount_payment,true);
		$criteria->compare('number_payment',$this->number_payment,true);
		$criteria->compare('number_card',$this->number_card,true);
		$criteria->compare('status_payment',$this->status_payment);
		$criteria->compare('errorMessage',$this->errorMessage,true);
		$criteria->compare('errorCode',$this->errorCode,true);
		$criteria->compare('cardholderName',$this->cardholderName,true);
		$criteria->compare('comment',$this->comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * @return array
     */
    public function getPaymentStatusNames(){
        return [
            'pending' => self::PAYMENT_PENDING,
            'waiting_for_capture' => self::PAYMENT_WAITING_FOR_CAPTURE,
            'succeeded' => self::PAYMENT_SUCCEEDED,
            'canceled' => self::PAYMENT_CANCELED,
        ];
    }
    
    /**
     * @return array
     */
    public function getPaymentStatusId($status){
        $data = $this->getPaymentStatusNames();
        return isset($data[$status]) ? $data[$status] : 'pending';
    }
        
    /**
     * @return array
     */
    public function getPaymentStatusList()
    {
        return [
            self::PAYMENT_PENDING => Yii::t('InshineModule.inshine', 'платеж создан и ожидает действий от клиента'),
            self::PAYMENT_WAITING_FOR_CAPTURE => Yii::t('InshineModule.inshine', 'Платеж оплачен, деньги авторизованы и ожидают списания'),
            self::PAYMENT_SUCCEEDED => Yii::t('InshineModule.inshine', 'Платеж успешно завершен'),
            self::PAYMENT_CANCELED => Yii::t('InshineModule.inshine', 'Платеж отменен'),
        ];
    }
    
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaymentsClients the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
