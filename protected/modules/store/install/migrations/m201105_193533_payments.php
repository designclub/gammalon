<?php

class m201105_193533_payments extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->createTable(
            '{{payments_clients}}',
            [
                'id'            => 'pk',
                'email'   => 'varchar(1000)',
                'phone'   => 'varchar(1000)',
                'name'   => 'varchar(1000)',
                'product_id'   => 'integer NOT NULL',
                'date_payment' => 'datetime NOT NULL',
                'amount_payment'   => 'decimal(10, 2) DEFAULT NULL',
                'number_payment'   => 'varchar(50) DEFAULT NULL',
                'number_card'          => 'varchar(30) DEFAULT NULL',
                'status_payment'        => "integer NOT NULL DEFAULT '0'",
                'errorMessage'         => 'varchar(1000) DEFAULT NULL',
                'errorCode'         => 'varchar(10) DEFAULT NULL',
                'cardholderName'    => 'varchar(1000) DEFAULT NULL',
                'comment'     => 'text DEFAULT NULL',
            ],
            $this->getOptions()
        );
	}

	public function safeDown()
	{
        $this->dropTableWithForeignKeys('{{payments_clients}}');
	}
}