<?php

use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

/**
 * StandertForm
 */
class LightForm extends CFormModel
{
    public $name;
    public $email;
    public $phone;
    public $body;
    public $product;
    public $key;
    public $verify;
    public $check;
    public $idProduct;
    public $price;

    public function rules()
    {
        return [
            ['phone', 'safe'],
            ['email', 'email'],
            ['name, key, verify, body, product, idProduct, price', 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'  => 'Имя',
            'email' => 'Ваш E-mail',
            'phone' => 'Телефон',
            'body'  => 'Сообщение',
            'product'  => '',
        ];
    }

    public function beforeValidate()
    {
        /*if (isset($_POST['g-recaptcha-response'])){
            if($_POST['g-recaptcha-response']=='') {
                $this->addError('verify', 'Пройдите проверку reCAPTCHA.');
            } else {
                $post = [
                    'secret' => Yii::app()->params['secretkey'],
                    'response' => $_POST['g-recaptcha-response'],
                ];

                $ch = curl_init('https://www.google.com/recaptcha/api/siteverify');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                $response = curl_exec($ch);
                curl_close($ch);

                $response = CJSON::decode($response);
                if (isset($response['success']) and isset($response['error-codes']) and $response['success']===false) {
                    $this->addError('verify', implode(', ', $response['error-codes']));
                }
            }
        }*/
        return parent::beforeValidate();
    }
    
    /**
     *
     */
    public function afterValidate(){
        
        
        /*$paymentClient = new PaymentsClients;
        $product = Product::model()->findByPk($id);
    
  
        if ($product === null) {
            throw new CHttpException(404, Yii::t('StoreModule.store', 'Page not found!'));
        }
                
        $transaction = Yii::app()->getDb()->beginTransaction();
        try {
            
            $paymentClient->setAttributes([
                'product_id' => $this->idProduct,
                'phone' => $this->phone,
                'email' => $this->email,
                'name' => $this->name,
                'amount_payment' => $product->getDiscountPrice()
            ]);
                    
            if (!$paymentClient->validate()) {
                return false;
            }
            
            $payment = Yii::app()->ycash->createPayment($paymentClient);
            
            if(!empty($payment)){
                $paymentClient->number_payment  = $payment->getId();
                $paymentClient->status_payment = $paymentClient->getPaymentStatusId($payment->getStatus());
                $paymentClient->save();
                $url = $payment->getConfirmation()->getConfirmationUrl();
            }

            $transaction->commit();
            
            Yii::app()->request->redirect($url);
            
        } catch (Exception $e) {
            $transaction->rollback();

            return false;
        }
*/
        parent::afterValidate();
    }
    
}
