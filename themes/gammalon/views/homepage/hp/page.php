<?php
/** @var Page $page */

if ($page->layout) {
    $this->layout = "//layouts/{$page->layout}";
}

$this->title = $page->title;
$this->breadcrumbs = [
    Yii::t('HomepageModule.homepage', 'Pages'),
    $page->title
];
$this->description = !empty($page->meta_description) ? $page->meta_description : Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = !empty($page->meta_keywords) ? $page->meta_keywords : Yii::app()->getModule('yupe')->siteKeyWords
?>
<div class="mainscreen">
    <div class="bg-left"></div>
    <div class="bg-right"></div>

    <div class="header__logo header__logo-abs">
        <?php if (Yii::app()->hasModule('contentblock')) : ?>
            <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'logo']); ?>
        <?php endif; ?>
    </div>

    <div class="header fl fl-ai-c">
        <div class="content-site fl fl-ai-c fl-jc-sb">
            <div class="header__logo header__logo-content">
                <?php if (Yii::app()->hasModule('contentblock')) : ?>
                    <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'logo']); ?>
                <?php endif; ?>
            </div>

            <div class="header__menu">
                <?php if (Yii::app()->hasModule('menu')) : ?>
                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu', 'view' => 'menu']); ?>
                <?php endif; ?>
            </div><!-- menu -->

            <div class="header__contacts fl fl-ai-c fl-jc-sb">
                <div class="phone">
                    <?php if (Yii::app()->hasModule('contentblock')) : ?>
                        <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'mode']); ?>
                    <?php endif; ?>
                    <?php if (Yii::app()->hasModule('contentblock')) : ?>
                        <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'phone']); ?>
                    <?php endif; ?>
                </div>
                <div class="callback">
                    <button class="btn btn-red" data-toggle="modal" data-target="#callbackModal">Сделать заказ</button>
                </div>
            </div>
            <div class="mobile-panel">
                <div class="m-menu-button">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>
            </div><!-- mobile-panel -->
        </div>

    </div>
    <div class="mobile-panel">
        <div class="mobile-menu">
            <div class="m-menu-buttons">
                <span class="line"></span>
                <span class="line"></span>
                <span class="line"></span>
            </div>
            <div class="mobile-content">
                <?php if (Yii::app()->hasModule('menu')) : ?>
                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu', 'view' => 'menu']); ?>
                <?php endif; ?>
            </div>
        </div>
    </div><!-- mobile-panel -->

    <div class="slide">
        <div class="content-site">
            <?php $this->widget("application.modules.page.widgets.PagesNewWidget", [
                'id'=> 2,
                'view' => 'slide'
            ]); ?>
        </div>
    </div>

    <div class="about" id="about">
        <div class="content-site">
            <?php $this->widget("application.modules.page.widgets.PagesNewWidget", [
                'id'=> 3,
                'view' => 'about'
            ]); ?>
        </div>
    </div>
</div>

<div class="complex pt pb" id="complex">
    <div class="content-site">
        <?php $this->widget("application.modules.page.widgets.PagesNewWidget", [
            'id'=> 4,
            'view' => 'complex'
        ]); ?>
    </div>
</div>

<div class="original" id="original">
    <div class="original-left"></div>
    <div class="original-right"></div>
    <div class="content-site">
        <?php $this->widget("application.modules.page.widgets.PagesNewWidget", [
            'id'=> 5,
            'view' => 'original'
        ]); ?>
    </div>
</div>

<div class="application pb" id="application">
    <div class="content-site">
        <?php $this->widget("application.modules.page.widgets.PagesNewWidget", [
            'id'=> 6,
            'view' => 'application'
        ]); ?>
    </div>
</div>

<div class="product" id="cost">
    <div class="content-site">
        <h2 class="heading">Стоимость препарата</h2>
        <div class="product-block fl fl-w">
            <?php $this->widget('application.modules.store.widgets.ProductWidget', [
                'view' => 'product'
            ]); ?>
        </div>
    </div>
</div>

<div class="reviews pt" id="reviews">
    <div class="content-site">
        <h2 class="heading">Отзывы тех, <br>кто уже пользуется <br>препаратом <span>Gammalon<sup>®</sup></span></h2>
        <div class="reviews-block fl fl-ai-c fl-jc-sb">
            <?php $this->widget("application.modules.page.widgets.PagesNewWidget", [
                'id'=> 7,
                'view' => 'reviews'
            ]); ?>
        </div>
    </div>
</div>

<div class="qustions bg-lightblue pt pb" id="qustions">
    <div class="content-site">
        <h2 class="heading">Часто задаваемые вопросы</h2>
        <?php $this->widget("application.modules.page.widgets.PagesNewWidget", [
            'id'=> 10,
            'view' => 'questions'
        ]); ?>
    </div>
</div>
