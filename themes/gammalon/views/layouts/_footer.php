<div class="footer">
    <div class="content-site">
        <div class="footer-panel fl fl-jc-sb">
            <div class="footer-logo item">
                <?php if (Yii::app()->hasModule('contentblock')): ?>
                <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'logo']); ?>
                <?php endif; ?>
                <div class="policy">
                    © <?= date('Y'); ?> "Gammalon"       
                </div>
            </div>

            <div class="footer-menu item">
                <?php if (Yii::app()->hasModule('menu')) : ?>
                <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu', 'view' => 'menu']); ?>
                <?php endif; ?>
            </div>

            <div class="footer-address item">
                <div class="address-top">
                    <?php if (Yii::app()->hasModule('contentblock')): ?>
                        <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'mode']); ?>
                    <?php endif; ?>
                    <?php if (Yii::app()->hasModule('contentblock')): ?>
                        <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'phone']); ?>
                    <?php endif; ?>
                </div>
                <div class="address-bottom">
                    <p>Пишите нам</p>
                    <?php if (Yii::app()->hasModule('contentblock')): ?>
                        <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'email']); ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="footer-contact item fl fl-d-c">  
                <p>Местонахождение в Японии</p>  
                <div class="phone">
                    <div class="location">
                        <?php if (Yii::app()->hasModule('contentblock')): ?>
                        <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'address']); ?>
                        <?php endif; ?>   
                    </div>
                    <button class="btn btn-red" data-toggle="modal" data-target="#callbackModal">Заказать сейчас</button> 
                </div>

                <div class="dc56">
                    <p>Создано в</p>
                    <a href="https://dcmedia.ru/"><span class="icon-DCMedia--"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span></a>
                </div>
            </div>
        </div>
    </div>
</div>




