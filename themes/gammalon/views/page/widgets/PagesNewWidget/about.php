<?php if($pages) : ?>
    <div class="about-info fl fl-ai-c">
	    <div class="about-info__text">
	        <h2 class="about-info__name heading"><?= $pages->title; ?></h2>
	        <div class="about-info__desc"><?= $pages->body; ?></div>
	    </div>
	    <div class="about-info__img">
	        <?= CHtml::image($pages->getImageUrl(), '',['class' => 'absolute-im']); ?>
	    </div>
	</div>
	<div class="about-desc">
		<div class="about-list fl fl-w fl-ai-fs">
			<?= $pages->body_short; ?>
		</div>
	</div>
<?php endif; ?>



