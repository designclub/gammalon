<?php if($pages) : ?>
    <h2 class="application__name heading"><?= $pages->title; ?></h2>
    <div class="application__age fl fl-w fl-jc-sb"><?= $pages->body; ?></div>
	<div class="application-desc">
		<div class="application-list fl fl-w fl-ai-fs">
			<?= $pages->body_short; ?>
		</div>
	</div>
<?php endif; ?>

