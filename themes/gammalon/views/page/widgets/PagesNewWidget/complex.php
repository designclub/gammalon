<?php if($pages) : ?>
    <div class="complex-info fl fl-ai-fe">
	    <div class="complex-info__text">
	        <h2 class="complex-info__name heading"><?= $pages->title; ?></h2>
	        <div class="complex-info__desc"><?= $pages->body; ?></div>
	    </div>
	    <div class="complex-info__img">
	        <?= CHtml::image($pages->getImageUrl(), '',['class' => 'absolute-im']); ?>
	    </div>
	</div>
	<div class="complex-desc">
		<div class="complex-list fl fl-w">
			<?= $pages->body_short; ?>
		</div>
	</div>
<?php endif; ?>



