<?php if($pages) : ?>
	<div class="reviews-slider">
		<div class="reviews-carousel slick-slider">
            <?php foreach ($pages->childPages(['condition' => 'status=1', 'order' => 'childPages.order ASC']) as $key => $data) : ?>
                <div>
                    <div class="reviews-carousel__item fl fl-d-c">
                        <div class="reviews-carousel__img fl fl-ai-c">
                            <div class="reviews-photo">
                            	<?= CHtml::image($data->getImageUrl(), '',['class' => 'absolute-img']); ?>
                            </div>
                            <div class="reviews-name">
                                <?= $data->title; ?><br>
                                <?= $data->body_short; ?>
                            </div>
                        </div>
                        <div class="reviews-carousel__body fl fl-d-c fl-jc-sb">
                           <?= $data->body; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="navigation fl fl-ai-c fl-jc-sb">
        	<div class="counter fl fl-ai-c"></div>
        	<div class="arrows fl fl-ai-c"></div>
        </div>
	</div>
	<div class="reviews-img">
		<img src="/uploads/image/img-7.png" alt="">
	</div>
<?php endif; ?>



