<?php if($pages) : ?>
	<h2 class="original__title heading"><?= $pages->title; ?></h2>
	<div class="original__img fl fl-ai-c fl-jc-c">
		<?= CHtml::image($pages->getImageUrl(), '',['class' => 'absolute-im']); ?>
	</div>
	<div class="original-video fl fl-jc-e">
		<div class="original-video__text">
			<h2 class="heading"><?= $pages->title_short; ?></h2>
			<div><button class="original-video__btn btn btn-red" data-toggle="modal" data-target="#callbackModal">Заказать сейчас</button></div>
		</div>
		<div class="original-video__box">
            <?php $this->widget('application.modules.video.widgets.VideoWidget', [
                'category_id' => $pages->id,
                'view' => 'proizvodstvo-video-home',
            ]); ?>
		</div>
	</div>
<?php endif; ?>



