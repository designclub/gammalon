<?php if($pages) : ?>
    <div class="slide-box fl fl-ai-c fl-jc-sb">
	    <div class="slide__text">
	        <h1 class="slide__name"><?= $pages->title; ?></h1>
	        <div class="slide__desc"><?= $pages->body; ?></div>
	        <div>
	        	<button class="slide__btn btn btn-red" data-toggle="modal" data-target="#learnMoreModal">Узнать подробнее</button>
	        </div>
	    </div>
	    <div class="slide__img">
	        <?= CHtml::image($pages->getImageUrl(), '',['class' => 'absolute-im']); ?>
	    </div>
    </div>
<?php endif; ?>

