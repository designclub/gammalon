<div class="product__item fl fl-jc-sb">
    <div class="product-info fl fl-d-c fl-jc-sb">
        <div>
            <div class="product__name fl fl-ai-c">
                <div class="name-num"><?= $data->name; ?></div>
                <div class="name-text">
                    <?= $data->description; ?>
                </div>
            </div>

            <div class="product__desc">
                <?= $data->data; ?>
            </div>
        </div>

        <div>
            <div class="product__price">
                <?php if ($data->discount_price): ?>
                    <?php if ($data->hasDiscount()): ?>
                        <div class="price price_old price_decor"><?= (float)$data->getBasePrice() ?><i class="fa fa-rub" aria-hidden="true"></i></div>
                    <?php endif; ?>
                    <div class="price price_actual">
                        <?= (float)$data->getDiscountPrice() ?> <i class="fa fa-rub" aria-hidden="true"></i>
                    </div>
                <?php else : ?>
                    <div class="price price_old price_only"><?= (float)$data->getBasePrice() ?><i class="fa fa-rub" aria-hidden="true"></i></div>
                <?php endif; ?>
            </div>

            <button class="btn btn-red js-modal-show" data-toggle="modal" data-target="#orderModal" data-name="<?= $data->name; ?>" data-product="<?= $data->id; ?>" data-description="<?= strip_tags($data->description); ?>" data-price="<?= !empty($data->discount_price) ? (float)$data->getDiscountPrice() : $data->getBasePrice(); ?>">Купить</button>
        </div>
    </div>

    <div class="product-image fl fl-d-c fl-jc-c">
        <?= CHtml::image($data->getImageUrl(), '',['class' => 'absolute-im']); ?>
    </div>
</div>
