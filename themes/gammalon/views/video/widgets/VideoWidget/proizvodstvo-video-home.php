<?php foreach ($models as $key => $model): ?>
    <div class="video-box__item">
        <?php if($model->video) : ?>
            <a class="video-play__link fl fl-ai-c fl-jc-c"  data-fancybox="iframe" href="<?= $model->getVideoUrl() ?>">
                <span class="icon-play"><span class="path1"></span><span class="path2"></span></span>
            </a>
        <?php endif; ?>
        <div class="video-box__img box-style-img">
            <?php if ($model->image): ?>
                <?= CHtml::image($model->getImageUrl(790,540,true), ''); ?>
            <?php else : ?>
                <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/video.jpg',''); ?>
            <?php endif; ?>
        </div>
    </div>
<?php endforeach; ?>

