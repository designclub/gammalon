<div class="video-box">
    <?php foreach ($models as $key => $model): ?>
        <div class="video-box__item">
            <div class="video-box-play">
                <a data-fancybox="iframe" href="<?= $model->code; ?>">
                    <div class="video-box-play__icon anim-play-mini"></div>
                </a>
            </div>
            <div class="video-box__img">
                <?php if ($model->image): ?>
                    <?= CHtml::image($model->getImageUrl(), $model->name, ['title' => $model->name]); ?>
                <?php else : ?>
                    <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/news-nophoto.jpg',''); ?>
                <?php endif; ?>
            </div>
            <div class="video-box__text">
                <div class="video-box__name">
                    <?= CHtml::encode($model->name); ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<script>
    //var nice = $(".video__right").niceScroll(); 
</script>
<?php $fancybox = $this->widget(
    'gallery.extensions.fancybox3.AlFancybox', [
        'target' => '[data-fancybox]',
        'lang'   => 'ru',
        'config' => [
            'animationEffect' => "fade",
            'buttons' => [
                "zoom",
                "close",
            ]
        ],
    ]
); ?>